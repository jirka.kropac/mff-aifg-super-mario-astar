#!/bin/bash

for ndw in $(seq -f "%0.2f" 0.2 0.2 2)
do
  for ttfw in $(seq -f "%0.2f" 0.2 0.2 2)
  do
    echo $ndw $ttfw
    qsub -v nodeDepthWeight=$ndw,timeToFinishWeight=$ttfw hw2_script.sh
  done
done