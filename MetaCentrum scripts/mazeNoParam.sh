#!/bin/bash
#PBS -l select=1:ncpus=1:mem=4gb:scratch_local=1gb:spec=5.1:cluster=adan
#PBS -l walltime=1:00:00
#PBS -e /storage/praha1/home/kropy25/logs
#PBS -o /storage/praha1/home/kropy25/logs

DATADIR=/storage/praha1/home/kropy25/mff-aifg-super-mario-astar
RESULTDIR=/storage/praha1/home/kropy25/results
ROOT=/storage/praha1/home/kropy25

echo "$PBS_JOBID is running on node `hostname -f` in a scratch directory $SCRATCHDIR" >> $DATADIR/jobs_info.txt

module add python/python-3.7.7-intel-19.0.4-mgiwa7z || { echo >&2 "Error in module add python"; exit 2; }

# test if scratch directory is set
# if scratch directory is not set, issue error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

cp -R $DATADIR/maze_astar $SCRATCHDIR
cd $SCRATCHDIR/maze_astar

python maze_astar_no_param.py

cp -a results/. $RESULTDIR

clean_scratch
