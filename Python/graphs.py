import csv
import os
from pydoc import plain

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from matplotlib import rcParams
from pandas.core.interchange.dataframe_protocol import DataFrame

rcParams.update({'figure.autolayout': True})

RESULT_FOLDER = '../Results_HW1/results'
GRAPH_FOLDER = '../Results_HW1/graphs'

win_rate = [[[0,0] for i in range(10)] for j in range(11)]
run_time = [[[0,0] for k in range(10)] for l in range(11)]

counter = 0
for file in os.listdir(RESULT_FOLDER):
    counter += 1
    if file.endswith('.csv'):
        header = pd.read_csv(os.path.join(RESULT_FOLDER, file), header=None, nrows=2)
        result = pd.read_csv(os.path.join(RESULT_FOLDER, file), skiprows=2, usecols=[1, 3])

        row = float(header.values[0][0].split(":")[1])
        col = float(header.values[1][0].split(":")[1])
        if row == 20: row = 11
        row = int(row - 1)
        col = int((col / 2) * 10 - 1)
        for res in result.values:
            win_rate[row][col][0] += res[0]
            win_rate[row][col][1] += 1
            run_time[row][col][0] += res[1]
            run_time[row][col][1] += 1



win_rate_heatmap = []
run_time_heatmap = []

for row in win_rate:
    heat = []
    for col in row:
        heat.append(col[0] / col[1])
    win_rate_heatmap.append(heat)

for row in run_time:
    heat = []
    for col in row:
        heat.append(col[0] / (col[1] * 1000))
    run_time_heatmap.append(heat)

win_rate = pd.DataFrame(win_rate_heatmap, index=[1,2,3,4,5,6,7,8,9,10,20], columns=[0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0])
win_rate.round(3)
#win_rate.pivot(index="searchSteps", columns="timeToFinish", values="win_rate")
win_rate_heatmap = sns.heatmap(win_rate, xticklabels=True, yticklabels=True,annot=True, cmap='crest', cbar_kws={'label': 'Win Rate'})
plt.title("Common task: Win rate based on TTFW and SS")
plt.xlabel("Time To Finish Weight")
plt.ylabel("Search Steps")
plt.show()

win_rate_fig = win_rate_heatmap.get_figure()
win_rate_fig.savefig(GRAPH_FOLDER + '/hw1_win_rate.png', format='png', bbox_inches="tight")

run_time = pd.DataFrame(run_time_heatmap, index=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20], columns=[0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0])
run_time.round(3)
run_time_heatmap = sns.heatmap(run_time, xticklabels=True, yticklabels=True, annot=True, cmap='crest', cbar_kws={'label': 'Run Time [s]'})
plt.title("Common task: Run time based on TTFW and SS")
plt.xlabel("Time To Finish Weight")
plt.ylabel("Search Steps")
plt.show()

run_time_fig = run_time_heatmap.get_figure()
run_time_fig.savefig(GRAPH_FOLDER + '/hw1_run_time.png', format='png', bbox_inches="tight")